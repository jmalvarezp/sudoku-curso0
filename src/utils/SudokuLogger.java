package utils;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class SudokuLogger {
    static Logger logger;
    public FileHandler fileHandler;
    SimpleFormatter plainText;

    private SudokuLogger() throws IOException{
        //instance the logger
        logger = Logger.getLogger(SudokuLogger.class.getName());
        //instance the file handler
        fileHandler = new FileHandler("./resources/sudokuLog.txt",false);
        //instance formatter, set formatting, and handler
        plainText = new SimpleFormatter();
        fileHandler.setFormatter(plainText);
        logger.addHandler(fileHandler);
    }
    
    private static Logger getLogger(){
        if(logger == null){
            try {
                new SudokuLogger();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return logger;
    }
    public static void log(Level level, String msg){
        //getLogger().log(level, msg);
    }
}
