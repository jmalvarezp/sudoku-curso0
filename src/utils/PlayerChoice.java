package utils;

public class PlayerChoice {
	private PlayerAction action;
	private int row;
	private int column;

	public PlayerChoice(PlayerAction act, int r, int c) {
		action = act;
		row = r;
		column = c;
	}

	public PlayerAction getAction() {
		return action;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}
	
	

}
