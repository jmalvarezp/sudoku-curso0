package view;

import java.util.Scanner;

import controller.GameConsoleControl;
import model.SudokuGrid;
import utils.PlayerAction;
import utils.PlayerChoice;

public class ConsoleView {

	SudokuGrid sudoku;
	GameConsoleControl control;
	
	Scanner keyboardScanner;
	
	public ConsoleView(SudokuGrid sg, GameConsoleControl gc) {
		sudoku = sg;
		control = gc;
		keyboardScanner = new Scanner(System.in);
	}
	
	public PlayerChoice getPlayerChoice() {
		char choice;
		int row = 0;
		int column = 0;
		do {
			System.out.print("Choose your action: (S)et a value/ (E)mpty a cell. ");
			String str = keyboardScanner.nextLine();
			System.out.println("");
			choice = Character.toUpperCase(keyboardScanner.next().charAt(0));
		}while (choice != 'S' && choice != 'E');

		do {
			System.out.println("Choose row (between 1 and 9: ");
			if (keyboardScanner.hasNextInt()){
				row = keyboardScanner.nextInt();
			} else {
				keyboardScanner.nextLine();
			}
			System.out.println("");
		}while (1 <= row && row <= 9);

		do {
			System.out.println("Choose column (between 1 and 9: ");
			if (keyboardScanner.hasNextInt()){
				column = keyboardScanner.nextInt();
			} else {
				keyboardScanner.nextLine();
			}
			System.out.println("");
		}while (1 <= column && column <= 9);

		PlayerChoice pc;
		if (choice == 'S') {
			pc = new PlayerChoice(PlayerAction.SETVALUE, row, column);
		} else {
			pc = new PlayerChoice(PlayerAction.EMPTYCELL, row, column);
		}
		
		return pc;
	}
	
	
	
}
