package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.GameController;
import model.CellState;
import model.SudokuGrid;

/**
 * Graphic element to represent a cell of the sudoku grid.
 * @author jmalvarez
 *
 */
public class CellPanel extends JPanel {

	private static final long serialVersionUID = 893850168959322739L;
	private static final Color CELL_COLOR = Color.WHITE;
	private static final Color CELL_EDITING_COLOR = Color.GRAY.brighter();
	private static final Color FIXED_TEXT_COLOR = Color.BLUE.darker();
	private static final Color REGULAR_TEXT_COLOR = Color.BLACK;
	private static final Color DUPLICATED_TEXT_COLOR = Color.RED;
	private static final int SIDE_LENGTH = 60;
	private static final int TEXT_SIZE = 30;
	private static final String TEXT_EMPTY_CELL = "";
	private static final String WRONG_KEY_MESSAGE = "Wrong key. Only values from 1 to 9 are allowed.";

	private JTextField cellValue;
	private String lastValue;
	
	private SudokuGrid gridModel;
	private int row, column;
	private BlockPanel containerBlockPanel;
	private GameController controller;
	boolean selected;

	/**
	 * Constructor. Cell parameters are set and listeners defined.
	 * @param sudokuGame
	 * @param r
	 * @param c
	 * @param bp
	 */
	public CellPanel(SudokuGrid sudokuGame, int r, int c, BlockPanel bp) {
		gridModel = sudokuGame;
		row = r;
		column = c;
		containerBlockPanel = bp;
		selected = false;

		Dimension prefSize = new Dimension(SIDE_LENGTH, SIDE_LENGTH);

		setBackground(CELL_COLOR);
		FlowLayout fl = (FlowLayout)this.getLayout();
		fl.setVgap(0);
		fl.setHgap(0);
		if (gridModel.cellState(row, column) == CellState.EMPTY) {
			lastValue = TEXT_EMPTY_CELL;
		} else {
			lastValue = Integer.toString(gridModel.getValueAt(row, column));
		}
		cellValue = new JTextField();
		cellValue.setBackground(CELL_COLOR);
		if (gridModel.cellState(row, column) == CellState.FIXED) {
			cellValue.setForeground(FIXED_TEXT_COLOR);
		} else {
			cellValue.setForeground(REGULAR_TEXT_COLOR);
		}
		cellValue.setHorizontalAlignment(SwingConstants.CENTER);
		cellValue.setPreferredSize(prefSize);
		cellValue.setFont(new Font("Helvetica Neue", Font.PLAIN, TEXT_SIZE));
		cellValue.setText(lastValue);
		add(cellValue);
		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		cellValue.setEditable(false);

		cellValue.addMouseListener(new MouseListener() {

			/**
			 * When mouse is clicked on a cell, it becomes selected.
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				selectCell();
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub		
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
			}});
		
		cellValue.addKeyListener(new KeyListener() {

			/**
			 * Keys between 1 and 9 sets a new value for the cell.
			 * Back space or delete keys remove value from the cell.
			 */
			@Override
			public void keyPressed(KeyEvent ke) {
				if (gridModel.cellState(row, column) != CellState.FIXED) {
					cellValue.setForeground(REGULAR_TEXT_COLOR);
					char key = ke.getKeyChar();
					if ('1' <= key && key <= '9') {
						cellValue.setText(Character.toString(key));
						lastValue = Character.toString(key);
					} else if(ke.getKeyCode() == KeyEvent.VK_BACK_SPACE || ke.getKeyCode() == KeyEvent.VK_DELETE) { 
						cellValue.setText(TEXT_EMPTY_CELL);
						lastValue = TEXT_EMPTY_CELL;
					} else {
						cellValue.setText("");
						JOptionPane.showMessageDialog(null, WRONG_KEY_MESSAGE);
					}
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
			}});
	}

	/**
	 * This method attaches the controller to the block and to the inner cells.
	 * @param gc. Controller to be attached.
	 */
	public void attachController(GameController gc) {
		controller = gc;
		
	}
	
	/**
	 * When a cell is selected, the enclosing grid is reported, so the 
	 * previously selected cell can be deselected.
	 */
	public void selectCell() {
		if (!selected) {
			selected = true;
			containerBlockPanel.newCellSelected(row, column);
			if (gridModel.cellState(row, column) != CellState.FIXED) {
				lastValue = cellValue.getText();
				setBackground(CELL_EDITING_COLOR);
			}
		}
	}
	
	/**
	 * When a cell is deselected, the controlled is reported about 
	 * its new state (empty or with a new value). If the new value 
	 * cannot fit in the cell, it is highlighted.
	 */
	public void deselect() {
		selected = false;
		if (lastValue.equals(TEXT_EMPTY_CELL)) {
			controller.emptyCell(row, column);
		} else {
			controller.setValueAt(Integer.valueOf(lastValue), row, column);
			if (gridModel.canFit(row, column)) {
				cellValue.setForeground(REGULAR_TEXT_COLOR);
			} else {
				cellValue.setForeground(DUPLICATED_TEXT_COLOR);
			}
		}
		setBackground(CELL_COLOR);
		cellValue.setText(lastValue);
	}
}
