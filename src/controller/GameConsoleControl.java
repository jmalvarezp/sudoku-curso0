package controller;

import java.util.Scanner;

import model.SudokuGrid;
import utils.PlayerAction;
import utils.PlayerChoice;


public class GameConsoleControl {

	SudokuGrid sgrid;
	Scanner keyboardScanner;

	
	public GameConsoleControl(SudokuGrid sg) {
		sgrid = sg;
		keyboardScanner = new Scanner(System.in);
	}
	
	public void play() {
		System.out.println(sgrid.toString());
		getPlayerChoice();
	}
	
	public PlayerChoice getPlayerChoice() {
		char choice = 'a';
		int row = 0;
		int column = 0;
		do {
			System.out.print("Choose your action: (S)et a value/ (E)mpty a cell: ");
			String str = keyboardScanner.nextLine();
			System.out.println("");
			choice = Character.toUpperCase(str.charAt(0));
		}while (choice != 'S' && choice != 'E');

		do {
			System.out.print("Choose row (between 1 and 9): ");
			if (keyboardScanner.hasNextInt()){
				row = keyboardScanner.nextInt();
			} else {
				keyboardScanner.nextLine();
			}
			System.out.println("");
		}while (row < 1 || 9 < row);

		do {
			System.out.print("Choose column (between 1 and 9): ");
			if (keyboardScanner.hasNextInt()){
				column = keyboardScanner.nextInt();
			} else {
				keyboardScanner.nextLine();
			}
			System.out.println("");
		}while (column < 1 || 9 < column);

		PlayerChoice pc;
		if (choice == 'S') {
			pc = new PlayerChoice(PlayerAction.SETVALUE, row, column);
		} else {
			pc = new PlayerChoice(PlayerAction.EMPTYCELL, row, column);
		}
		
		return pc;
	}

}
