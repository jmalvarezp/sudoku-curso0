package main;

import javax.swing.SwingUtilities;

import controller.GameController;
import model.SudokuGrid;
import view.GameFrame;

public class SudokuGraphicMain {

	public static void createAndShowGUI() {
		
		SudokuGrid sudokuGame = SudokuGrid.createNewGame();
		GameFrame sudokuFrame = new GameFrame("Sudoku", sudokuGame);
		GameController controller = new GameController(sudokuGame, sudokuFrame);
		sudokuFrame.attachController(controller);
		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		      createAndShowGUI();
		    }
		  });

	}

}
