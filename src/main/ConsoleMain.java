package main;

import java.util.Scanner;

import controller.GameConsoleControl;
import model.SudokuGrid;
import utils.PlayerAction;
import utils.PlayerChoice;
import view.ConsoleView;

public class ConsoleMain {

	SudokuGrid sudoku = new SudokuGrid();
	
	static Scanner keyboardScanner;


	public static void main (String args[]) {

		SudokuGrid sudoku = new SudokuGrid();

		sudoku.fillSudokuGrid(true);
		
		GameConsoleControl gControl = new GameConsoleControl(sudoku);
		
//		ConsoleView cView = new ConsoleView(sudoku, gControl);
		
		gControl.play();

	}

}
