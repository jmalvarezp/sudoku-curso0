Sudoku is a puzzle game where the user has to fill a 9x9 grid of cells with numbers between 1 and 9, in such a way that no number is repeated in any row, any column, or any block of 3x3 elements.

This program generates a partially filled sudoku grid and the user has to fill in the empty cells. When the user clicks the right button of the mouse on a cell, the cell becomes selected and the user can insert a new value or removes a value previously inserted. Cells filled in the initial grid cannot be changed.

If the value inserted by the user is repeated in the cell's row, column, or block, the cell is highlighted, so the user can notice the value is wrong.

When the grid is full with a valid solution, the program pops up a congratulation message.

